//Nav dep
import { Outlet, Routes, Route } from "react-router-dom";
//Layout
import Footer from "../Components/Footer/Footer";
import Navbar from "../Components/Navbar/NavBar";
//Page
import Home from "../Page/Home/Home";
import AboutUs from "../Page/AboutUs/AboutUs";
import BelanjaSekarang from "../Page/BelanjaSekarang/BelanjaSekarang";
import CoverageArea from "../Page/CoverageArea/CoverageArea";

function BasicLayout() {
  return (
    <>
      <Navbar />
      <Outlet />
      <Footer />
    </>
  );
}

function Navigation() {
  return (
    <>
      <Routes>
        <Route path="/" element={<BasicLayout />}>
          <Route index element={<Home />} />
          <Route path="belanja-sekarang" element={<BelanjaSekarang />} />
          <Route path="coverage-area" element={<CoverageArea />} />
          <Route path="about-us" element={<AboutUs />} />
        </Route>
      </Routes>
    </>
  );
}

export default Navigation;
