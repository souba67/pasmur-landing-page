import React from "react";
import Discount from "../../Components/Discount/Discount";
import MainCard from "../../Components/MainCard/MainCard";
import Trending from "../../Components/Trending/Trending";

function Home() {
  return (
    <>
      <MainCard />
      <Discount />
      <Trending />
    </>
  );
}

export default Home;
