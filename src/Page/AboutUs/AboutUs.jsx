import React from "react";
import AboutUsCard from "../../Components/AboutUsCard/AboutUsCard";
import ServiceInfoCard from "../../Components/ServiceInfoCard/ServiceInfoCard";
import VisionCard from "../../Components/VisionCard/VisionCard";

function AboutUs() {
  return (
    <>
      <AboutUsCard />
      <VisionCard />
      <ServiceInfoCard />
    </>
  );
}

export default AboutUs;
