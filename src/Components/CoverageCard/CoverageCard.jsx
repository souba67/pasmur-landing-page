import React from "react";
import Logo from "../../Assets/PrimaryLogo.png";
import Pict from "../../Assets/CurrentLocation.svg";

// import { Fragment, useState } from "react";
// import { Listbox, Transition } from "@headlessui/react";
// import { CheckIcon, ChevronUpDownIcon } from "@heroicons/react/20/solid";

const coverage = [
  {
    id: 1,
    name: "Bekasi Utara",
    link: "https://pasar-murah-nusantara.sirclo.me/id",
  },
  { id: 2, name: "Cikeas", link: "https://pasar-murah-nusantara.sirclo.me/id" },
  {
    id: 3,
    name: "Keramat Jati",
    link: "https://pasar-murah-nusantara.sirclo.me/id",
  },
];

// const Dropdown = () => {
//   const [selected, setSelected] = useState(coverage[0]);
//   const [query, setQuery] = useState("");

//   const filteredBranch =
//     query === ""
//       ? coverage
//       : coverage.filter((branch) => {
//           return branch.toLowerCase().includes(query.toLowerCase());
//         });

//   return (
//     <>
//       <Listbox value={selected} onChange={setSelected}>
//         <div className="w-72 relative border border-pasmur-orange-base h-14 cursor-default rounded-xl ">
//           <Listbox.Button className="cursor-pointer h-12 rounded-lg bg-white py-2 pl-3 pr-5 text-left focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
//             <span className="block truncate text-pasmur-orange-base font-semibold text-base">
//               {selected.name}
//             </span>
//             <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
//               <ChevronUpDownIcon
//                 className="h-5 w-5 text-pasmur-orange-base"
//                 aria-hidden="true"
//               />
//             </span>
//           </Listbox.Button>
//           <Transition
//             as={Fragment}
//             leave="transition ease-in duration-100"
//             leaveFrom="opacity-100"
//             leaveTo="opacity-0"
//           >
//             <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg focus:outline-none sm:text-sm">
//               {coverage.map((branch, id) => (
//                 <Listbox.Option
//                   key={id}
//                   className={({ active }) =>
//                     `relative cursor-default select-none py-2 pl-10 pr-4 ${
//                       active
//                         ? "bg-amber-100 text-pasmur-orange-base"
//                         : "text-gray-500"
//                     }`
//                   }
//                   value={branch}
//                   placeholder="Pilih area terdekat!"
//                 >
//                   {({ selected }) => (
//                     <>
//                       <span
//                         className={`block truncate ${
//                           selected ? "font-semibold" : "font-normal"
//                         }`}
//                       >
//                         {branch.name}
//                       </span>
//                       {selected ? (
//                         <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
//                           <CheckIcon className="h-5 w-5" aria-hidden="true" />
//                         </span>
//                       ) : null}
//                     </>
//                   )}
//                 </Listbox.Option>
//               ))}
//             </Listbox.Options>
//           </Transition>
//         </div>
//       </Listbox>
//     </>
//   );
// };

const ScrolableCoverageArea = () => {
  return (
    <div class="flex flex-col bg-white m-auto p-auto">
      <h1 class="flex py-5 lg:px-20 md:px-10 px-5 lg:mx-40 md:mx-20 mx-5 font-bold text-4xl text-gray-800">
        Coverage Area
      </h1>
      <p className="flex py-5 lg:px-20 md:px-10 px-5 lg:mx-40 md:mx-20 mx-5 text-base text-gray-500 md:text-xl">
        Click untuk mulai berbelanja
      </p>
      <div class="flex overflow-x-scroll pb-10 hide-scroll-bar">
        <div class="flex flex-nowrap lg:ml-40 md:ml-20 ml-10 ">
          {coverage.map((data) => {
            return (
              <div key={data.id} class="inline-block px-3">
                <div class="w-64 h-24 max-w-xs overflow-hidden rounded-lg shadow-md bg-gray hover:shadow-xl transition-shadow duration-300 ease-in-out">
                  <span className=" block text-center truncate text-pasmur-green-base font-semibold text-base">
                    <a
                      href={data.link}
                      className="inline-block justify-center content-center"
                    >
                      Cabang {data.name}
                      <img
                        src={Logo}
                        width={120}
                        height={80}
                        objectFit="contain"
                        alt="Discount!"
                        className=""
                      ></img>
                    </a>
                  </span>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

function CoverageCard() {
  return (
    <>
      <div className=" w-screen">
        <div className="flex h-auto w-screen items-center justify-center p-8">
          <div className="ml-10 md:ml-20 md:w-1/2">
            <h1 className="my-5 text-5xl font-bold md:text-7xl  text-pasmur-green-base ">
              Cek <span className="text-pasmur-orange-base">lokasimu!</span>
            </h1>
            <h1 className="my-5 text-5xl font-bold  text-pasmur-green-base md:text-7xl">
              Mulai <span className="text-pasmur-orange-base">belanja!</span>
            </h1>
            <p className="text-base text-gray-500 md:text-xl">
              Beli eceran harga grosiran!
            </p>
            <div className="mt-12 flex items-start justify-start gap-5 text-center ">
              {/* <Dropdown /> */}
            </div>
          </div>

          <div className="hidden md:block animate-[wiggle_1s_ease-in-out_infinite]">
            <img
              src={Pict}
              width={900}
              height={700}
              objectFit="contain"
              alt="hero img"
            />
          </div>
        </div>
        <ScrolableCoverageArea />
      </div>
    </>
  );
}

export default CoverageCard;
