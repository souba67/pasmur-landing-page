import React from "react";
import { StarIcon } from "@heroicons/react/24/solid";
import { Link } from "react-router-dom";
import Bawang from "../../Assets/BawangMerah.jpeg";
import Telur from "../../Assets/Telur.jpeg";

function Trending() {
  return (
    <div className="my-20 flex  items-center justify-center">
      {/* images section  */}
      <div className="flex w-11/12 flex-col items-center justify-center gap-4 p-5 md:ml-20 md:flex-row">
        <div className="relative">
          <img
            src={Bawang}
            alt="first"
            width={400}
            height={300}
            objectFit="cover"
            className="rounded-xl shadow-md"
          />
          <div className=" flex h-auto w-full flex-col items-start justify-start rounded-xl bg-white pl-4 shadow-xl">
            <p className="mt-5 text-sm font-thin uppercase text-gray-500">
              Bawang
            </p>
            <h2 className="text-3xl font-bold text-gray-900">
              Bawang Merah Berebes
            </h2>
            <div className="mb-5 mt-2 flex items-center justify-center">
              <StarIcon className="text-yellow-600" />
              <p className=" pl-2 text-sm text-gray-500"> 5.0 (12 reviews)</p>
            </div>
          </div>
        </div>
        <div className="relative">
          <img
            src={Telur}
            alt="second"
            width={400}
            height={300}
            objectFit="cover"
            className="rounded-xl shadow-md"
          />
          <div className="flex h-auto flex-col items-start justify-start rounded-xl bg-white pl-4 shadow-xl">
            <p className="mt-5 text-sm font-thin uppercase text-gray-500">
              Telur
            </p>
            <h2 className="text-3xl font-bold text-gray-900">
              Telur Ayam Organik
            </h2>
            <div className="mb-5 mt-2 flex items-center justify-center">
              <StarIcon className="text-yellow-600" />
              <p className=" pl-2 text-sm text-gray-500"> 4.8 (48 reviews)</p>
            </div>
          </div>
        </div>
        {/* textual content */}
        <div className="flex-col items-start justify-start sm:mt-8 md:mt-0 md:ml-20">
          <h1 className="text-5xl font-bold text-gray-900">
            Produk Terlaris Minggu Ini!
          </h1>
          <p className="mt-5 w-3/4 text-base text-gray-600 md:text-xl">
            Mari bantu perekonomian petani! Taukah kamu dengan memebeli barang
            dipasar murah kami membanti petani dengan menaikan harga jual mereka
            dan memeberikan mereka akses langsung ke pembeli
          </p>
          <div className="flex items-start justify-start gap-5">
            <div className="my-5 flex-col">
              <h1 className="text-4xl font-bold text-gray-600">10+</h1>
              <p className="text-xl font-medium text-gray-600">Kota</p>
            </div>
            <div className="my-5 flex-col">
              <h1 className="text-4xl font-bold text-gray-600">800+</h1>
              <p className="text-xl font-medium text-gray-600">Mitra</p>
            </div>
            <div className="my-5 flex-col">
              <h1 className="text-4xl font-bold text-gray-600">300+</h1>
              <p className="text-xl font-medium text-gray-600">Petani</p>
            </div>
          </div>
          <button className="h-14 cursor-pointer rounded-xl  border border-pasmur-orange-base px-8 font-semibold text-pasmur-orange-base hover:bg-pasmur-green-base hover:text-white hover:shadow-xl hover: border-back">
            <Link to="belanja-sekarang">Belanja Sekarang!</Link>
          </button>
        </div>
      </div>
    </div>
  );
}

export default Trending;
