import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/20/solid";

const arrayNarasi = [
  {
    id: 1,
    judul: "Bagaimana cara belanja di PasarMurah?",
    narasi:
      "Kami hadir di beberapa platform e-commerce dan pemesanan, bisa  juga dapat dilakukan melalui web dengan petunjuk secara lebih detail.",
  },
  {
    id: 2,
    judul: "Apakah PasarMurah menerima pembayaran dengan system COD",
    narasi:
      "Untuk produk tertentu dan channel e-commerce tertentu kami menyediakan pembayaran dengan system COD",
  },
  {
    id: 3,
    judul: "Berapa Ongkir PasarMurah",
    narasi:
      "Anda dapat mengetahui tariff ongkir melalui biaya layanan ekspedisi  yang akan anda gunakan.",
  },
  {
    id: 4,
    judul: "Apakah ada min. transaksi belanja di PasarMurah?",
    narasi:
      "Anda dapat berbelanja sesuai dengan kebutuhan tanpa ada minimum berbelanja, namun jika ingin mendapatkan harga terbaik silahkan baca di syarat dan ketentuan yang berlaku.",
  },
  {
    id: 5,
    judul: "Dimana area layanan ?",
    narasi:
      "Kami melayani seluruh area, namun untuk produk FMCG terbatas hanya untuk area sekitar Mitra Paman untuk menjaga kualitas produk yang kami  kirimkan.",
  },
  {
    id: 6,
    judul: "Apakah belanja di PasarMurah aman?",
    narasi:
      "PasarMurah berdiri dalam naungan PT PasarMurah Nusantara yang dibentuk sesuai dengan anggaran dasar dan tercatat dalam Nomer Induk Berusaha (NIB) sesuai dengan ketentuan perundang-undangan yang berlaku.",
  },
  {
    id: 7,
    judul: "Bagaimana pengecekan kualitas pada produk PasarMurah?",
    narasi:
      "Setiap produk yang akan akan dikirimkan ke pelanggan sudah melalui tahap Quality Control untuk memberikan kualitas berbelanja yang baik.",
  },
  {
    id: 8,
    judul: "Apakah ada batasan maks, jumlah produk yang di beli di PasarMurah?",
    narasi:
      "Kami melayani pembelian partai besar maupun partai kecil tanpa adanya batasan pembelian.",
  },
];

export default function ServiceInfoCard() {
  return (
    <div className="flex flex-col px-4 pt-16">
      <div className="mx-auto w-full max-w-4xl rounded-2xl bg-white p-2">
        <h1 class="flex py-5 font-bold text-4xl text-gray-800">FAQ</h1>
        {arrayNarasi.map((data, id) => (
          <Disclosure key={id} as="div" className="mt-2">
            {({ open }) => (
              <>
                <Disclosure.Button className="flex w-full justify-between rounded-lg bg-amber-600 px-4 py-2 text-left text-sm font-medium text-white hover:bg-amber-500 focus:outline-none focus-visible:ring focus-visible:ring-amber-500 focus-visible:ring-opacity-75">
                  <span>{data.judul}</span>
                  <ChevronUpIcon
                    className={`${
                      open ? "rotate-180 transform" : ""
                    } h-5 w-5 text-white`}
                  />
                </Disclosure.Button>
                <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                  {data.narasi}
                </Disclosure.Panel>
              </>
            )}
          </Disclosure>
        ))}
      </div>
    </div>
  );
}
