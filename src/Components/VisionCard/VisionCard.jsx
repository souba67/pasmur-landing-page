import React from "react";
import Pict1 from "../../Assets/1.svg";
import Pict2 from "../../Assets/2.svg";
import Pict3 from "../../Assets/3.svg";

const arrData = [
  {
    id: 1,
    judul: "Mengedepankan pengalaman belanja terbaik",
    narasi:
      "PasarMurah hadir dalam genggaman anda dengan kemudahan akses yang kami tawarkan. PasarMurah menawarkan pengalaman berbelanja yang mengutamakan kenyamanan dan kemudahaan dengan komitmen memberikan pelayanan yang mudah, cepat dan harga yang bersaing untuk memenuhi kebutuhan belanja harian anda.",
    path: Pict1,
  },
  {
    id: 2,
    judul: "Mengajak tumbuh dan maju bersama PasarMurah",
    narasi:
      "Akses untuk supplier agar menjangkau pelanggan yang lebih luas, serta mendigitalisasi Merchant Agent PasarMurah sehingga memiliki mode pembayaran yang lengkap, dan mampu memasarkan dengan cakupan area yang lebih luas dibantu dengan pengiriman yang handal",
    path: Pict2,
  },
  {
    id: 3,
    judul: "Memberikan dampak positif bagi komunitas",
    narasi:
      "PasarMurah berusaha untuk menjadi bagian dari komunitas masayarakat, untuk keberlangsungan lingkungan sekitar.",
    path: Pict3,
  },
];

function VisionCard() {
  return (
    <>
      {arrData.map((data, id) => (
        <div className="mt-20 flex flex-wrap items-center mx-6">
          <div key={id} className="w-full sm:w-1/2 text-center sm:px-6">
            <h3 className="text-2xl  text-pasmur-orange-base font-semibold">
              {data.judul}
            </h3>
            <div className="text-base text-gray-500 md:text-xl">
              {data.narasi}
            </div>
          </div>
          <div className="w-full sm:w-1/2 p-6">
            <img src={data.path} width={350} height={350} alt="pasar murah" />
          </div>
        </div>
      ))}
    </>
  );
}

export default VisionCard;
