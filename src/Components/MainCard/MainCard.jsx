import React from "react";
import { Link } from "react-router-dom";
import Pict from "../../Assets/baseCard.svg";

function MainCard() {
  return (
    <>
      <div className=" w-screen">
        <div className="flex h-auto w-screen items-center justify-center p-8">
          <div className="ml-10 md:ml-20 md:w-1/2">
            <h1 className="my-5 text-5xl font-bold md:text-7xl  text-pasmur-green-base ">
              Bela<span className="text-pasmur-orange-base">nja</span>
            </h1>
            <h1 className="my-5 text-5xl font-bold  text-pasmur-green-base md:text-7xl">
              Seka<span className="text-pasmur-orange-base">rang!</span>
            </h1>
            <p className="text-base text-gray-500 md:text-xl">
              Bahan segar berkualitas langsung dibawa ke rumah anda!
            </p>
            <div className="mt-12 flex items-start justify-start gap-5 text-center ">
              <button className=" h-14  cursor-pointer rounded-xl bg-pasmur-green-base px-8 font-semibold text-white hover:shadow-xl">
                <Link to="belanja-sekarang">Lihat Jangkauan</Link>
              </button>
            </div>
          </div>

          <div className="hidden md:block">
            <img
              src={Pict}
              width={900}
              height={700}
              objectFit="contain"
              alt="hero img"
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default MainCard;
