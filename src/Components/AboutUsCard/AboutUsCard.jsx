import React from "react";
import Pict from "../../Assets/aboutUs.svg";

function AboutUsCard() {
  return (
    <>
      <div className=" w-screen">
        <div className="flex h-auto w-screen items-center justify-center p-8">
          <div className="ml-10 md:ml-20 md:w-1/2">
            <h1 className="my-5 text-5xl font-bold md:text-7xl  text-pasmur-green-base ">
              Ten<span className="text-pasmur-orange-base">tang</span>
            </h1>
            <h1 className="my-5 text-5xl font-bold  text-pasmur-green-base md:text-7xl">
              Ka<span className="text-pasmur-orange-base">mi</span>
            </h1>
            <p className="text-base text-gray-500 md:text-xl">
              Bahan segar berkualitas langsung dibawa ke rumah anda!
            </p>
          </div>

          <div className="hidden md:block">
            <img
              src={Pict}
              width={900}
              height={700}
              objectFit="contain"
              alt="hero img"
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default AboutUsCard;
