import React from "react";
import { Link } from "react-router-dom";
import Pict from "../../Assets/Discount.svg";

function Discount() {
  return (
    <div className="mx-auto my-20 flex h-auto w-11/12 flex-col items-center justify-center rounded-md bg-pasmur-orange-base p-8 shadow-xl">
      <h1 className="w-3/4 text-center text-5xl font-semibold text-white md:text-6xl">
        Jangan lewatkan 50% Discount jika kamu belanja sekarang{" "}
      </h1>
      <div className="hidden md:block">
        <img
          src={Pict}
          width={500}
          height={400}
          objectFit="contain"
          alt="Duscount!"
        />
      </div>
      <button className="h-14 rounded-md border border-white px-10 text-xl text-white hover:border-none hover:bg-pasmur-green-base ">
        <Link to="belanja-sekarang">Daftar</Link>
      </button>
    </div>
  );
}

export default Discount;
